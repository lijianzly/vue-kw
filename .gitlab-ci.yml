stages:
  - install
  - test
  - build
  - deploy
  - notify

default:
  cache:
    - key: $CI_COMMIT_BRANCH
    - paths:
      - node_modules
    
variables:
  IMAGE_NAME: $CI_REGISTRY_IMAGE
  DEPLOY_SERVER_HOST: 101.200.218.201
  DEPLOY_SERVER_USER: root
  DEV_ENDPOINT: http://ec2-35-180-46-122.eu-west-3.compute.amazonaws.com:3000
  STAGING_SERVER_HOST: 35.180.46.122
  STAGING_ENDPOINT: http://ec2-35-180-46-122.eu-west-3.compute.amazonaws.com:4000
  PROD_SERVER_HOST: 35.180.46.122
  PROD_ENDPOINT: http://ec2-35-180-46-122.eu-west-3.compute.amazonaws.com:5000

# 安装依赖任务
job_install:
  stage: install
  tags:
    - docker
  image: node:18.17.0-alpine3.18
  script: 
    - npm config set registry https://registry.npmmirror.com
    - npm install -g pnpm
    - pnpm install
# 编写流水线，要根据项目灵活修改。比如现在这个 vue3 项目，它是使用 vite 创建的。
# 默认的 build 命令，它会先使用 vue-tsc（类似于 tsc） 检查代码，再使用 vite build 打包
# 检查代码规范的步骤，应该是在本地 commit 时做，还是放到 CI/CD 中去做?

# 测试任务
job_test:
  stage: test
  tags:
    - docker
  image: node:18.17.0-alpine3.18
  # cache: 
  #   key: $CI_COMMIT_BRANCH
  #   paths:
  #     - node_modules
  script:
    - npm config set registry https://registry.npmmirror.com
    - npm install -g pnpm
    - pnpm install
    # - pnpm test:ci

# 构建任务
job_build_dist:
  stage: build
  tags:
    - docker
  image: node:18.17.0-alpine3.18
  script: 
    - npm config set registry https://registry.npmmirror.com
    - npm install -g pnpm
    - pnpm install
    - pnpm run build
  artifacts:
    paths:
      - dist/

# 打包成 docker 镜像
job_build_image:
  stage: build
  only:
    - main
    # - master
  tags:
    - shell
  needs:
    - job_build_dist
  before_script:
    # 在宿主机上运行的，已经安装了 jq，可以用来解析JSON
    - export IMAGE_TAG=$(cat package.json | jq -r .version)
    # 将版本号保存为环境变量
    - echo "IMAGE_TAG=$IMAGE_TAG" >> build.env
  artifacts:
    reports: # 将环境变量导出为制品
      dotenv: build.env
  script: 
    - docker build -t $CI_REGISTRY_IMAGE:$IMAGE_TAG .
    - docker tag $CI_REGISTRY_IMAGE:$IMAGE_TAG $CI_REGISTRY_IMAGE:latest

# 推送镜像到镜像库中
job_push_image:
  stage: build
  only:
    # - master
    - main
  needs:
    - job_build_image
  tags:
    - shell
  before_script:
    # 不再需要再次读取 package.json 了，直接使用环境变量即可
    # - export IMAGE_TAG=$(cat package.json | jq -r .version)
    - docker login $CI_REGISTRY -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD
  script:
    - ls
    - echo $IMAGE_TAG
    - docker push $CI_REGISTRY_IMAGE:$IMAGE_TAG
    - docker push $CI_REGISTRY_IMAGE:latest

# 部署任务1：部署到 GitLab Pages
pages:
  stage: deploy
  only:
    - master
  when: manual
  cache: {}
  tags:
    - shell
  script:
    # 这里有个坑，vue项目根目录下已经有一个 public 了
    # 因此不能这样直接拷贝
    # - cp -r dist public
    # 会使用项目本身的public目录
    # 解决：先删除原来的 public，再新建一个新的public
    - rm -rf public
    - cp -r dist public
  artifacts:
    paths:
      - public/
  environment: production
    
# 部署任务2：使用 Docker 部署
job_deploy_docker:
  stage: deploy
  only:
    - master
  tags:
    - shell
  cache: {}
  dependencies:
    - job_build_image
  before_script:
    - chmod 400 $SSH_PRIVATE_KEY
  script:
    - echo $IMAGE_NAME
    - echo $IMAGE_TAG
    - echo $SERVER_HOST
    - scp -i $SSH_PRIVATE_KEY -o StrictHostKeyChecking=no ./docker-compose.yml $DEPLOY_SERVER_USER@$DEPLOY_SERVER_HOST:/vue-kw
    - ssh -i $SSH_PRIVATE_KEY -o StrictHostKeyChecking=no $DEPLOY_SERVER_USER@$DEPLOY_SERVER_HOST "
        cd /vue-kw &&
        export DC_IMAGE_NAME=$IMAGE_NAME && 
        export DC_IMAGE_TAG=$IMAGE_TAG &&
        export DC_APP_PORT=3000 &&
        docker compose down &&
        docker compose up -d 
        "
    
# 部署任务3：部署到服务器
job_deploy_server:
  stage: deploy
  when: manual
  only:
    - master
  tags:
    - docker
  image: ubuntu:20.04
  before_script:
    - 'command -v ssh-agent >/dev/null || ( apt-get update -y && apt-get install openssh-client -y )'
    - eval $(ssh-agent -s)
    - echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add -
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    - ssh-keyscan $DEPLOY_SERVER >> ~/.ssh/known_hosts
    - chmod 644 ~/.ssh/known_hosts
  script: 
    - echo "部署开始"
    - scp -r dist/* root@$DEPLOY_SERVER:/vue-project
    - echo "部署完成"

# 部署任务4：部署到阿里云OSS
job_deploy_oss:
  stage: deploy
  only:
    - master
  when: manual
  tags:
    - docker
  image: ubuntu:20.04
  script: 
    - echo "部署开始"
    - apt-get update -y && apt-get install curl -y
    # 安装 unzip 来解压 ossutil
    - apt-get install unzip -y
    # 下载并安装 ossutil
    - curl https://gosspublic.alicdn.com/ossutil/install.sh | bash
    # 配置 ossutil 并上传文件
    - ossutil cp -r -f dist oss://vue-kw/ -e $OSS_END_POINT -i $OSS_ACCESS_KEY_ID -k $OSS_ACCESS_KEY_SECRET
    # - ossutil cp -r -f dist oss://vue-kw/ -e $OSS_END_POINT -i $OSS_ACCESS_KEY_ID -k $OSS_ACCESS_KEY_SECRET -L CH –-loglevel debug 
    # 使用 ossutil 上传文件
    # - ossutil  cp -r -f dist oss://vue-kw/
    - echo "部署到阿里云OSS完成"

notify_success:
  stage: notify
  only:
    - master
  needs:
    - pages
    # - job_deploy_docker
    # - job_deploy_server
    # - job_deploy_oss
  tags:
    - docker
  image: ubuntu:20.04
  before_script:
    - which ssh-agent || (apt-get update -y && apt-get install -y curl telnet)
  script:
       - "curl -X POST -H \"Content-Type: application/json\" -d '{\"msg_type\":\"text\",\"content\":{\"text\":\"GitLab 流水线构建成功\"}}' $WebHook"

notify_fail:
  stage: notify
  tags:
    - docker
  image: ubuntu:20.04
  before_script:
    - which ssh-agent || (apt-get update -y && apt-get install -y curl telnet)
  script:
    - "curl -X POST -H \"Content-Type: application/json\" -d '{\"msg_type\":\"text\",\"content\":{\"text\":\"GitLab 流水线构建失败\"}}' $WebHook"
  when: on_failure